# -*- coding: utf-8; mode: org-development-elisp; -*-
#+title: ort-macs
#+subtitle: Supplementary stuff for ORT needed at build time
#+author: =#<PERSON akater A24961DE3ADD04E057ADCF4599555CE6F2E1B21D>=
#+property: header-args :tangle ort-macs.el :lexical t
#+startup: nologdone show2levels
#+todo: TODO(t@) HOLD(h@/!) | DONE(d@)
#+todo: BUG(b@/!) | FIXED(x@)
#+todo: TEST(u) TEST-FAILED(f) | TEST-PASSED(p)
#+todo: DEPRECATED(r@) | OBSOLETE(o@)

#+begin_src elisp :results none
(require 'cl-lib)
#+end_src

* string-whitespace-free-p
** Prerequisites
*** whitespace-char-p
#+begin_src elisp :results none
(defsubst whitespace-char-p (char)
  (cl-member char '(?\s ?\t) :test #'char-equal))
#+end_src

** Definition
#+begin_src elisp :results none
(defsubst string-whitespace-free-p (string)
  (cl-flet ((whitespace-char-p (x) (whitespace-char-p x)))
    (not (cl-some #'whitespace-char-p string))))
#+end_src

** Tests
*** TEST-PASSED A single whitespace
#+begin_src elisp :tangle no :results code :wrap example elisp
(string-whitespace-free-p " ")
#+end_src

#+EXPECTED:
#+begin_example elisp
nil
#+end_example

*** TEST-PASSED Empty string
#+begin_src elisp :tangle no :results code :wrap example elisp
(string-whitespace-free-p "")
#+end_src

#+EXPECTED:
#+begin_example elisp
t
#+end_example

*** TEST-PASSED Non-empty string without whitespace
#+begin_src elisp :tangle no :results code :wrap example elisp
(string-whitespace-free-p "abc")
#+end_src

#+EXPECTED:
#+begin_example elisp
t
#+end_example

*** TEST-PASSED Non-empty string with trailing whitespace
#+begin_src elisp :tangle no :results code :wrap example elisp
(string-whitespace-free-p "abc ")
#+end_src

#+EXPECTED:
#+begin_example elisp
nil
#+end_example

* with-properties
** Dependencies
#+begin_src elisp :results none
(require 'mmxx-macros-anaphora)
(require 'mmxx-macros-defmacro)
#+end_src

** Definition
#+begin_src elisp :results none
(defmacro-mmxx with-properties (properties plist &body body &gensym 0 plist)
  `(let ,(amapcar (cond
                   ((and (consp it) (consp (cdr it)))
                    `(,(car it) (cl-getf ,plist
                                         ,(keyword (car it)) ,(cadr it))))
                   (t `(,it (cl-getf ,plist ,(keyword it)))))
                  properties)
     ,@body))
#+end_src
