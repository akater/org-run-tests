;;;  -*- lexical-binding: t -*-

(defconst-with-prefix fakemake
  feature 'ort
  authors "Dima Akater"
  first-publication-year-as-string "2021"
  org-files-in-order '("ort-macs"
                       "ort")
  org-files-for-testing-in-order nil
  site-lisp-config-prefix "50"
  license "GPL-3")
